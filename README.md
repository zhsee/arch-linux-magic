# to enable wireless network

- sudo systemctl start NetworkManager.service
- sudo systemctl disable systemd-network-generator.service
- sudo systemctl disable systemd-networkd-wait-online.service
- sudo systemctl disable systemd-networkd-wait-online@.service
- sudo systemctl disable systemd-networkd.service
- sudo rfkill unblock all
- sudo ip link set wlp0s20f3: up
- sudo nmcli device wifi connect <SSID> password <PASSWD>

# deep sleep to preserve battery
- egrep LINE_LINUX_DEFAULT /etc/default/grub
- GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 pci=noaer quiet splash mem_sleep_default=deep"
- sudo grub-mkconfig -o /boot/grub/grub.cfg

# unblock bluetooth
- sudo cat /var/lib/connman/settings
```
    [Bluetooth]
    Enable=true
```


